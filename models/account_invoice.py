# -*- coding: utf-8 -*-
###############################################################################
#    License, author and contributors information in:                         #
#    __openerp__.py file at the root folder of this module.                   #
###############################################################################

from openerp import models, fields, api
from openerp.tools.translate import _
from openerp.exceptions import except_orm, Warning
from datetime import datetime, date, time, timedelta
import logging

class account_invoice(models.Model):

    _inherit = 'account.invoice'
    _logger = logging.getLogger(__name__)

    # --------------------------- ENTITY  FIELDS ------------------------------

    move_lines_denied = fields.Many2many(
        string='Move lines denied',
        required=False,
        readonly=False,
        index=False,
        default=None,
        help=False,
        comodel_name='account.move.line',
        relation='account_invoice_account_move_line_denied',
        column1='account_move_line_id',
        column2='account_invoice_id',
        domain=[],
        context={},
        limit=None
    )

    # ------------------------ METHODS  OVERWRITTEN ---------------------------

    def action_debit_denied(self):
        new_move_lines = []
        for record in self:
            new_move_lines = self._create_account_move(record.id)
            self._check_invoice_and_remove_reconcile(record.id)
            if self.test_paid():
                raise except_orm(
                    _('Error !'),
                    _("You cannot set invoice '%s' to state 'debit "
                      "denied', as it is still reconciled.") % record.number)
        self.write({
            'state': 'debit_denied',
            'move_lines_denied': [(6, 0, new_move_lines)]
        })
        for inv_id, name in self.name_get():
            message = _("Invoice '%s': direct debit is denied.") % name
            self.log(inv_id, message)
        return True

    # ----------------------------- NEW METHODS -------------------------------

    @api.model
    def _set_actual_period(self):
        for period in self.env['account.period'].\
           search([('special', '=', False)]):
            date_start_period = fields.Datetime.from_string(period.date_start)
            date_end_period = fields.Datetime.from_string(period.date_stop)
            if datetime.now() >= date_start_period and \
                datetime.now() <= date_end_period:
                return period[0] or period

    @api.multi
    def _check_invoice_and_remove_reconcile(self, invoice_id):
        invoice = self.env['account.invoice']\
            .search([('id', '=', invoice_id)])
        for move_line_id in invoice.move_id.line_id:
            if move_line_id.reconcile_ref:
                self.env['account.move.line']\
                    ._remove_move_reconcile(move_ids=[move_line_id.id])

    def _create_account_move(self, invoice_id):
        new_move_lines = []
        invoice = self.env['account.invoice']\
            .search([('id', '=', invoice_id)])
        for payment_id in invoice.payment_ids:
            move_id = payment_id.move_id
            move_line_ids = self.env['account.move.line']\
                .search([('move_id', '=', move_id.id)])
            new_move = self.env['account.move'].create({
                'name': '/',
                'date': date.today(),
                'partner_id': move_id.partner_id.id,
                'company_id': move_id.company_id.id,
                'journal_id': move_id.journal_id.id,
                'state': 'draft',
                'period_id': self._set_actual_period().id,
                'invoice': move_id.invoice.id,
                'picking': move_id.picking.id
            })
            for move_line_id in move_line_ids:
                if move_line_id.debit == 0:
                    account_account = self.env['account.account'].search([
                        ('code', 'like', '4315'),
                        ('type', '=', 'receivable')
                    ])[0] or move_line_id.account_id
                else:
                    account_account = move_line_id.account_id
                new_line_id = self.env['account.move.line'].create({
                    'move_id': new_move.id,
                    'name': '/',
                    'date': date.today(),
                    'partner_id': new_move.partner_id.id,
                    'ref': move_line_id.ref,
                    'statement_id': move_line_id.statement_id.id,
                    'tax_amount': move_line_id.tax_amount,
                    'account_id': account_account.id,
                    'credit': move_line_id.debit,
                    'debit': move_line_id.credit,
                    'invoice': new_move.invoice.id
                })
                if move_line_id.debit == 0:
                    new_move_lines.append(new_line_id.id)
        return new_move_lines
